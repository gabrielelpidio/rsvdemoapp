const express = require("express");

const response = require("../network/response");
const Store = require("../store/mysql");
const Controller = require("./index");

const router = express.Router();

router.get("/:table", list);
router.get("/:table/:id", get);
router.post("/:table", insert);
router.put("/:table", update);
router.post("/:table/query", query);
router.delete("/:table/:id", del);

async function list(req, res, next) {
  const data = await Store.list(req.params.table);
  response.success(req, res, data, 200);
}

async function get(req, res, next) {
  const data = await Store.get(req.params.table, req.params.id);
  response.success(req, res, data, 200);
}

async function insert(req, res, next) {
  const datos = await Store.insert(req.params.table, req.body);
  response.success(req, res, datos, 200);
}

async function update(req, res, next) {
  const data = await Store.update(req.params.table, req.body);
  response.success(req, res, data, 200);
}

async function query(req, res, next) {
  const data = await Store.query(
    req.params.table,
    req.body.query,
    req.body.join
  );
  response.success(req, res, data, 200);
}

async function del(req, res, next) {
  const data = await Store.del(req.params.table, req.params.id);
  response.success(req, res, data, 200);
}

module.exports = router;
