const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const config = require("../config.js");
const user = require("./components/user/network");
const auth = require("./components/auth/network");
const errors = require("../network/errors");

const app = express();

//Router
app.use(cors());
app.use(bodyParser.json());
app.use("/api/auth", auth);
app.use("/api/user", user);
app.use(errors);
app.listen(config.api.port, () => {
  console.log("Api is listening on port", config.api.port);
});
