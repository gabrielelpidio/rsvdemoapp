const TABLE = "auth";
const bcrypt = require("bcrypt");
const auth = require("../../../auth");
const err = require("../../../utils/error");

module.exports = function (injectedStore) {
  let store = injectedStore;
  if (!store) {
    store = require("../../../store/dummy");
  }

  async function upsert(data) {
    const authData = {
      id: data.id,
    };

    if (data.username) {
      authData.username = data.username;
    }
    if (data.password) {
      authData.password = await bcrypt.hash(data.password, 5);
    }

    return store.upsert(TABLE, authData, data.method);
  }

  function del(id) {
    return store.del(TABLE, id);
  }

  async function login(username, password) {
    const data = await store.query(TABLE, { username: username });
    if (!data) {
      throw err("Unauthorized", 401);
    }
    return bcrypt.compare(password, data.password).then((areEqual) => {
      if (areEqual === true) {
        // token
        return { token: auth.sign(data), id: data.id };
      } else {
        throw err("Unauthorized", 401);
      }
    });
  }

  return {
    upsert,
    login,
    del,
  };
};
