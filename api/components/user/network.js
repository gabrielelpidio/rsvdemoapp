const express = require("express");
const secure = require("./secure");
const checkUserExists = require("./middlewares");

const response = require("../../../network/response");

const Controller = require("./index");
const store = require("../../../store/remote-mysql");

const router = express.Router();
router.get("/", list);
router.get("/:id", get);
router.post("/", checkUserExists, upsert);
router.put("/", secure("update"), upsert);
router.delete("/:id", secure("update"), del);

function list(req, res, next) {
  Controller.list()
    .then((list) => {
      response.success(req, res, list, 200);
    })
    .catch(next);
}

function get(req, res, next) {
  Controller.get(req.params.id)
    .then((user) => {
      response.success(req, res, user, 200);
    })
    .catch(next);
}

function upsert(req, res, next) {
  console.log("upsert");
  Controller.upsert({ body: req.body, method: req.method })
    .then((user) => {
      response.success(req, res, user, 201);
    })
    .catch(next);
}

function del(req, res, next) {
  Controller.del(req.params.id)
    .then((user) => {
      response.success(req, res, user, 200);
    })
    .catch(next);
}

module.exports = router;
