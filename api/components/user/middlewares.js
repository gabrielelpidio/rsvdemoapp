const config = require("../../../config");

let store;
if (config.remoteDB === true) {
  store = require("../../../store/remote-mysql");
} else {
  store = require("../../../store/mysql");
}

async function checkUserExists({ body: { username } }, res, next) {
  const data = await store.query("user", { username: username });
  if (data) {
    throw new Error("User already exist");
  } else {
    next();
  }
}

function asyncHandler(fn) {
  return (req, res, next) => {
    fn(req, res, next).catch(next);
  };
}

module.exports = asyncHandler(checkUserExists);
