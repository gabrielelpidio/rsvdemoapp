const request = require("request");

function createRemoteDB(host, port) {
  const URL = "http://" + host + ":" + port;

  function list(table) {
    return req("GET", table);
  }

  function get(table, id) {
    return req("GET", table, id);
  }

  function insert(table, data) {
    return req("POST", table, data);
  }

  function update(table, data) {
    return req("PUT", table, data);
  }

  function upsert(table, data, method) {
    if (method === "PUT") {
      return update(table, data);
    }
    if (method === "POST") {
      return insert(table, data);
    }
  }

  function del(table, id) {
    return req("DELETE", table, id);
  }

  function query(table, query, join) {
    return req("POST", table + "/query", { query, join });
  }

  function req(method, table, data) {
    let url = URL + "/" + table;
    let body = "";

    if ((method === "GET" || method === "DELETE") && data) {
      // console.log(data);
      const id = data;
      url += "/" + id;
    } else if (data) {
      body = JSON.stringify(data);
    }

    return new Promise((resolve, reject) => {
      // console.log("url", url);
      // console.log("body", body);
      request(
        {
          method,
          headers: {
            "content-type": "application/json",
          },
          url,
          body,
        },
        (err, req, body) => {
          console.log(body);
          if (err) {
            console.error("Remote database error", err);
            return reject(err.message);
          }

          const resp = JSON.parse(body);
          return resolve(resp.body);
        }
      );
    });
  }

  return {
    list,
    get,
    upsert,
    query,
    del,
  };
}

module.exports = createRemoteDB;
